<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

if (function_exists('theatreevents_get_upcoming_posts')) { $posts = theatreevents_get_upcoming_posts(array('showposts'=> 1)); }

get_header(); 


?>

<div class="row clearfloats" id="column-container">

	<div id="content" class="narrowcolumn left">
	<?php //select first spotlighted event
	if (have_posts()) : $postIndex = 0; ?>

		<?php foreach ($posts as $post) : setup_postdata($post); ?>

			<div <?php post_class() ?> id="spotlight">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<img class="right" src="<?php echo str_replace('bedlamtheatre.org/images', 'bedlamtheatre.org', get_the_image_thumb('h=220&w=250&zc=1')); ?>" />
				</a>
				<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>				
				<h4 class="date-range"><?php if (function_exists('theatreevents_the_date_range')) { theatreevents_the_date_range(); } ?></h4>
				<div class="entry">
										<?php the_excerpt('Read the rest of this entry &raquo;'); ?>
					
				</div>
				<p>
					<a href="<?php the_permalink() ?>">More info, tickets and such</a>
				</p>
				<!--p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p-->
			</div>
			
			<?php 
				if ($postIndex == 0) : 
					//$socialpost
					//disable sidebar social display
					$GLOBALS['disp_social_banner']=0;
					if (function_exists('get_bedlam_social_banner')) { get_bedlam_social_banner();}
					else {
			?>
				<div id="home-banner-social" post="">
					<a href="pages/social">
				<h3>The Bedlam Social</h3>
				<em>new ways to feel, see and be theatre</em>    	
				</a>
				</div>
			<?php }
			endif; ?>
			

		<?php $postIndex++; endforeach; ?>

		

	<?php else : ?>

		<p>More to come, check back;</p>

	<?php endif; ?>
	
	
	<?php
		$subcolumnCategories = array(
			array(
				'category' => 'theatre',
				'title' => 'Upcoming Theatre'
			),
			array(
				'category' => 'music',
				'title' => 'Upcoming Music'
			),
			array(
				'category' => 'community-and-social',
				'title' => 'Upcoming Community & Social Events'
			)
		);
		
		foreach ($subcolumnCategories as $subColumn) :
	?>
	
		<div id="upcoming-<?php echo $subColumn['category']; ?>" class="subcolumn left">
		<?php /* echo $subColumn['title']; */
		$imgString = '/images/upcoming'.$subColumn['category'].'.png';
		?>
		<img src="<?php bloginfo('stylesheet_directory'); echo $imgString; ?>" alt="<?php echo $subColumn['title']; ?>" />
		<?php 
			if (function_exists('theatreevents_get_upcoming_events_by_tag_or_category_name')){
				$posts = theatreevents_get_upcoming_events_by_tag_or_category_name(array('category_name' => $subColumn['category'], 'showposts' => 5));
			}
			if (count($posts)) : 
		?>

			<?php foreach ($posts as $post) : setup_postdata($post); ?>
						
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					<br /><em><?php if (function_exists('theatreevents_the_date_range')) { theatreevents_the_date_range(); } ?></em>
					
					<!--p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p-->
				</div>
	
			<?php endforeach; ?>

		<?php else : ?>
	
			<p>More to come, check back;</p>
	
		<?php endif; ?>		
		</div>
		
	<?php endforeach; // end of $subColumn ?>
		<div class="row clearabove"> </div>
		
		<div id="donation-banner" class="left" style="background-image:url(<?php echo bedlamtheatre_randomBackground('donate'); ?>);">
			<a href="/page/donate">
				<img src="<?php bloginfo(stylesheet_directory); ?>/images/donatetext.png" alt="DONATE TO BEDLAM" />
				<br />
we make it with YOU! <br />donate, volunteer, participate!</a>
		</div>
		
		<div id="news-box" class="left">
			<h3><img src="<?php bloginfo(stylesheet_directory); ?>/images/latestnews.png" alt="Latest News:" /></h3>
			<?php query_posts('category_name=News&posts_per_page=1');
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						
 						<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>				
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><img class="left" src="<?php echo str_replace('bedlamtheatre.org/images', 'bedlamtheatre.org', get_the_image_thumb('h=50&w=280&zc=1')); ?>" /></a>
						<div class="entry">
							<?php the_excerpt('Read the rest of this entry &raquo;'); ?>
						</div>
						<a href="/category/news">READ ALL NEWS</a>
						<?php 
				endwhile; else:
			?> NO NEWS LIKE NO NEWS <?php 
				endif;
				wp_reset_query();
			?>

			
		
		</div>
		
		<div id="dont-miss-box" class="right">
			<h3><img src="<?php bloginfo(stylesheet_directory); ?>/images/anddontmiss.png" alt="And Don't Miss..." /></h3>
	 	
			<?php 
				if (function_exists('theatreevents_get_upcoming_posts')){
					$posts = theatreevents_get_upcoming_posts(array('tag' => 'dont-miss', 'showposts' => 1));
				}
				if (count($posts)) : 
			?>
			<?php foreach ($posts as $post) : setup_postdata($post); ?>
						
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img class="left" src="<?php echo str_replace('bedlamtheatre.org/images', 'bedlamtheatre.org', get_the_image_thumb('h=220&w=100&zc=1')); ?>" /></a>
					<em><?php if (function_exists('theatreevents_the_date_range')) { theatreevents_the_date_range(); } ?></em>
					<div class="entry">
										<?php the_excerpt('Read the rest of this entry &raquo;'); ?>
					</div>
					<p>
						<a href="<?php the_permalink() ?>">More info, tickets and such</a>
					</p>
					<!--p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p-->
				</div>
			<?php endforeach; ?>
		<?php else : ?>
	
			<p>More to come, check back;</p>
		<?php endif; ?>	
	 	</div>
	</div>

<?php get_sidebar(); ?>



</div>
<?php get_footer(); ?>