<?php 
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
 
if (!session_id()) {
    session_start();
}

if (function_exists('register_sidebar'))
    register_sidebar(array('before_widget'=>'<li id="%1$s" class="widget %2$s">', 'after_widget'=>'</li>', 'before_title'=>'<h2 class="widgettitle">', 'after_title'=>'</h2>', ));
    
if (is_admin()) {
    require_once (TEMPLATEPATH.'/code/controlpanel.php');
//    require_once (TEMPLATEPATH.'/code/datacopier.php');
    $cpanel = new ControlPanel();
//    $datacopier = new DataCopier();
}


function bedlamtheatre_randomBackground($category = '') {
    $themeRandomDirectory = '/images/random_head/';
    switch ($category) {
        case 'social':
            $themeRandomDirectory = '/images/random_head/';
            break;
        case 'donate':
        	$themeRandomDirectory = '/images/random_donate_banner/';
        	break;
    }
    
    if (isset($_SESSION['randomBackgrounds'.$category])) {
        $files = $_SESSION['randomBackgrounds'.$category];
    } else {
        $dirRandom = TEMPLATEPATH.$themeRandomDirectory;
        $files = scandir($dirRandom);
        $_SESSION['randomBackgrounds'.$category] = $files;
    }
    
    $r = array_rand($files, 1);
    $selectedFile = $files[$r];
    while (is_dir(get_bloginfo('template_directory').$themeRandomDirectory.$selectedFile) || substr($selectedFile, 0, 1) == '.' || !$selectedFile) {
        unset($_SESSION['randomBackgrounds'.$category][$r]);
        $r = array_rand($files, 1);
        $selectedFile = $files[$r];
    }
    
    return get_bloginfo('template_directory').$themeRandomDirectory.$files[$r].'?count='.count($files).'&r='.$r;
    
}

if (!function_exists('pr')) {
	function pr($output = false) {
	    if ($output) {
	        echo '<pre>';
	        print_r($output);
	        echo '</pre>';
	    }
	}
}

function get_the_image_path($options) {
	$defaults = array('default_size'=>'full', 'echo'=>false, 'url'=>true);
    $options = wp_parse_args($options, $defaults);
	// regex to get just URI
	return preg_replace('/.*src=\"(.*?)\".*/', '$1', get_the_image($options));
}

function get_the_image_thumb($arg, $options = '') {
    /*
     * Depends on two plugins, Get The Image, AutoThumb
     */
    
   
    $img = get_the_image_path($options);
    if (function_exists('getphpthumburl')) {
        if ($img == "<!-- No images were added to this post. -->") {
            echo "";
        } else {
            return getphpthumburl($img, $arg);
        }
    }
}
?>
