<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Links
*/
?>

<?php get_header(); ?>
<div class="row clearfloats">

    <div id="content" class="widecolumn">
    
    <h2>Links:</h2>
    <ul>
    <?php wp_list_bookmarks(); ?>
    </ul>
    
    </div>

</div>

<?php get_footer(); ?>
