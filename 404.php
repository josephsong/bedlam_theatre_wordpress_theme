<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>
<div id="column-container" class="row clearfloats">
	<div id="content" class="narrowcolumn left">

		<h2 class="center">Error 404 - Not Found</h2>

	</div>

	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>