<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
	<div id="sidebar" class="left">
		
		<?php if (function_exists('theatreevents_show_calendar')) { echo theatreevents_show_calendar(array('period' => 'week')); } ?>
		<?php 
		if ($GLOBALS['disp_social_banner'] !== 0) : 
		?>
		<div id="socialBanner"> <a href="/social-eat-drink/">
			<h3>The Bedlam Social</h3>
<em>new ways to feel, see and be theatre</em> 	
		</a>
</div><?php endif; ?>

	</div>

