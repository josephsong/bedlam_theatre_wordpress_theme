<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>

<div class="row clearfloats" id="column-container">

	<div id="content" class="narrowcolumn left">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
		
			<div class="entry">
				<h2><?php the_title(); ?></h2>
				<a href="<?php echo get_the_image_path(array('default_size' => 'full', 'url' => true)); ?>" rel="bookmark" title="click for larger image">
					<img id="page_image" class="right" src="<?php echo str_replace('bedlamtheatre.org/images', 'bedlamtheatre.org', get_the_image_thumb('h=200&w=695&zc=1')); ?>" />
				</a>
				<h4 class="date-range right"><?php theatreevents_the_date_range(); ?></h4>
				<div class="clearabove"> </div>
				<div class="row">
				
				<div class ="littlepart right">
				
				<?php if (function_exists('theatreevents_the_event_dates')) { theatreevents_the_event_dates(); } ?>
				</div>
				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
				
				
				</div>
				
				<p class="postmetadata alt hidden">
					<small>
						This entry was posted
						<?php /* This is commented, because it requires a little adjusting sometimes.
							You'll need to download this plugin, and follow the instructions:
							http://binarybonsai.com/archives/2004/08/17/time-since-plugin/ */
							/* $entry_datetime = abs(strtotime($post->post_date) - (60*120)); echo time_since($entry_datetime); echo ' ago'; */ ?>
						on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?>
						and is filed under <?php the_category(', ') ?>.
						You can follow any responses to this entry through the <?php post_comments_feed_link('RSS 2.0'); ?> feed.

						<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Both Comments and Pings are open ?>
							You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(); ?>" rel="trackback">trackback</a> from your own site.

						<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Only Pings are Open ?>
							Responses are currently closed, but you can <a href="<?php trackback_url(); ?> " rel="trackback">trackback</a> from your own site.

						<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Comments are open, Pings are not ?>
							You can skip to the end and leave a response. Pinging is currently not allowed.

						<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Neither Comments, nor Pings are open ?>
							Both comments and pings are currently closed.

						<?php } edit_post_link('Edit this entry','','.'); ?>

					</small>
				</p>

			</div>
			
		</div>
			<?php /*	<div id="home-banner-social" post="">
					<a href="/social-eat-drink/">
				<h3>While you're here, grab something from the Bedlam Social!</h3>
				Food and drinks like you like, <br />
				Tuesday - Sunday: 4pm - close	    	
				</a>
				</div> */ ?>
	<?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

	</div>
<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
