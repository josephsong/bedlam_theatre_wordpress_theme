<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<div class="clearabove"> </div>
<div id="footer">
	<a href="<?php echo get_option('home'); ?>/">home</a>
	<?php /* <a href="<?php echo get_option('home'); ?>/category/events">events</a> */ ?>
	<a href="<?php echo get_option('home'); ?>/pages/donate">donate</a>
	<a href="<?php echo get_option('home'); ?>/pages/social">social</a>
	<a href="<?php echo get_option('home'); ?>/pages/about">inside bedlam</a>
	<a href="<?php echo get_option('home'); ?>/category/news">news</a>
	<a href="<?php echo get_option('home'); ?>/pages/contact">contact</a>
	
	<a href="<?php echo get_option('home'); ?>/pages/directions">directions to bedlam</a>
	
	<br />
	
	&copy; <?php echo date('Y'); ?> Bedlam Theatre <?php $btOptions = get_option('bedlamtheatre'); echo $btOptions['telephone']; ?>
</div>
</div>


<?php wp_footer(); ?>
</body>
</html>
