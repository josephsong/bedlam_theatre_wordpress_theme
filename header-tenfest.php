<?php
/**
 * @package WordPress
 * @subpackage bedlam_theatre
 */
	if (!isset($_SESSION)) {
		session_start();
	}
	$menu_categories = array('Events', 'Social', 'Inside Bedlam', 'News', 'Contact');
	
	$randomBgCategory = '';
	if (is_category('social')) {
		$randomBgCategory = 'social';
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="description" content="Bedlam Theatre is a non-profit focusing on producing radical works of theater with a focus on collaboration and a unique blend of professional and community art." />
<link rel="image_src" href="<?php bloginfo('stylesheet_directory'); ?>/images/bedlam_email_logo.gif" />
<title>20.10 fest | Bedlam Theatre</title>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/structure.css" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/menu.css" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/screen.css" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/tenfest.css" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body>
<div id="page">


<div id="header">
    <div id="header-inner">
		<?php /* if(get_bloginfo('description')) { ?>
	    <div class="description courier"><?php bloginfo('description'); ?></div>
	    <?php } */ ?>
		<a href="<?php echo get_option('home'); ?>/tenfest"><h1><?php bloginfo('name'); ?></h1></a>
	</div>

<div id="menuContainer">
	<ul class="right">
		<?php /* foreach ($menu_categories as $cat) { $cat_id = get_cat_ID( $cat ); if ($cat_id) { ?>
		<li><a href="<?php echo get_category_link ( $cat_id ); ?>"><?php echo $cat; ?></a> 
		<?php /*
			switch ($cat) {
				case 'Events':
					echo '<ul>';
					wp_list_categories(array('child_of' => $cat_id, 'hide_empty' => 0, 'title_li' => ''));
					echo '</ul>';
					break;
			}*/
		?>
		
	<li><a href="https://www.brownpapertickets.com/event/105786" class="hightlight">buy tickets</a></li>
	<li><a href="<?php echo get_option('home'); ?>/tenfest/shows">shows</a></li>
	<li><a href="<?php echo get_option('home'); ?>/">bedlam</a></li>
	<li><a href="<?php echo get_option('home'); ?>/directions">directions</a></li>
	<li><a href="<?php echo get_option('home'); ?>/pages/contact">contact</a>
		</li>
		<?php //}} ?>
		<li></li>
	</ul>

</div>



	
	<br class="clearabove" />
</div>
