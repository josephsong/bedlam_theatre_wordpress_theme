<?php
class ControlPanel
{
    var $default_settings = array (
	    'address'=>'',
	    'telephone'=>''
    );
    function ControlPanel()
    {
        add_action('admin_menu', array ( & $this, 'admin_menu'));

        if (function_exists('register_sidebar'))
        register_sidebar( array ('name'=>'Sidebar'));

        if (!is_array(get_option('bedlamtheatre')))
        add_option('bedlamtheatre', $this->default_settings);
        $this->options = get_option('bedlamtheatre');
    }

    function admin_menu()
    {
        add_options_page('Bedlam Settings', 'Bedlam Settings', 'edit_themes', "bedlamtheatre", array ( & $this, 'optionsmenu'));
    }

    function optionsmenu()
    {
        if ($_POST['action'] == 'update')
        {
            $this->options["address"] = $_POST['address'];
            $this->options["telephone"] = $_POST['telephone'];
            update_option('bedlamtheatre', $this->options);
            echo '<div class="updated fade" id="message" style="background-color: rgb(255, 251, 204); width: 300px; margin-left: 20px"><p>Settings <strong>saved</strong>.</p></div>';
        }

?>
	<div class='wrap' id='bedlamtheatre-options-form'>
		<h2><?php _e('Bedlam Theatre Theme Options') ?></h2>
		<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
			<input type="hidden" name="action" value="update" />

			<h3>Contact Info</h3>
			<table class='form-table'>
				<tr>
					<th>
						<label for='address'><?php _e("Address: ") ?></label>
					</th>
					<td>
						<input type="text" size="60" name="address" id='address' value="<?php echo $this->options['address']; ?>" />
					</td>
				</tr>
			
				<tr>
					<th>
						<label for='telephone'><?php _e("Telephone: ") ?></label>
					</th>
					<td>
						<input type="text" size="60" name="telephone" id='telephone' value="<?php echo $this->options['telephone']; ?>" />
					</td>
				</tr>
			
			</table>

			<p class="submit">
				<input type="submit" name="Submit" value="<?php _e('Update Options', 'events-category' ) ?>" />
			</p>
		</form>
	</div>
<?php 
    }
}
