<?php
class DataCopier
{
    var $categories = array(
		'call' => 'calls',
		'guest' => 'guest-productions',
		'halloween' => 'guest-productions',
		'production' => 'bedlam-productions',
		'romp' => 'romps',
		'tenminute' => 'ten-minute-play-festival',
		'news' => 'news',
		'social' => 'social',
		'about-us' => 'about-us'
	);
	var $files = array();
	var $categoryNumbers = array();
	var $dbFrom = 'bedlamth_btmain';
	var $successfulCount = 0;
	var $errorCount = 0;
	var $errors = array();
	var $attachmentCount = 0;
	var $attachments = array();
	var $posts = array();
	var $eventDates = array();
	var $users = array();
	var $successfulUsersCount = 0;
	
    function DataCopier() {
        add_action('admin_menu', array ( & $this, 'admin_menu'));
		wp_enqueue_style('bedlam_dataconverter_admin', get_bloginfo('template_url') . '/css/admin.css');

        if (function_exists('register_sidebar'))
        register_sidebar( array ('name'=>'Sidebar'));
    }

    function admin_menu() {
        add_options_page('Copy Bedlam Data', 'Copy Bedlam Data', 'edit_themes', "bedlamtheatre-datacopier", array ( & $this, 'optionsmenu'));
    }
	
	function optionsmenu() {
        if (isset($_POST['action']) && $_POST['action'] == 'update') {
?>
	<div id="bedlam-data-converter-output">
<?php
        	$this->_convertOldDbData();
?>
	</div>
<?php
        }

?>
	<div class='wrap' id='bedlamtheatre-options-form'>
		<h2><?php _e('Copy Bedlam Theatre data from old site') ?></h2>
		<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
			<input type="hidden" name="action" value="update" />

			<p class="submit">
				<input type="submit" name="Submit" value="<?php _e('Copy Data', 'events-category' ) ?>" />
			</p>
		</form>
	</div>
<?php 
    }
	
	
	function _convertOldDbData() {
		
		ini_set('max_execution_time', 3600);
		ini_set("memory_limit","64M");
		
		include('ApproximateSearch.php');
		
		mysql_select_db(DB_NAME);
		
		// prepare old data
		echo 'Getting files ...<br />';
		@ob_flush(); flush();
    	$this->_getFiles();
//		pr($this->files);
//		die();
		echo 'Getting categories ...<br />';
		@ob_flush(); flush();
		$this->_getCategories();
		
		mysql_select_db($this->dbFrom);
		
		echo 'Converting events ...<br />';
		@ob_flush(); flush();
		$this->_convertEvents();
		echo 'Converting news ...<br />';
		@ob_flush(); flush();
		$this->_convertNews();
		echo 'Converting pages ...<br />';
		@ob_flush(); flush();
		$this->_convertPages();
		echo 'Converting users ...<br />';
		@ob_flush(); flush();
		$this->_convertUsers();
//		echo '<pre>';
//		print_r($this->files);
//		print_r($this->attachments);
		//die();
		mysql_select_db(DB_NAME);
		
		// insert new data
		echo 'Clearing old attachments and dates ...<br />';
		@ob_flush(); flush();
		$this->_clearExistingAttachmentsAndDates();
		echo 'Inserting posts ...<br />';
		@ob_flush(); flush();
		$this->_insertPosts();
		echo 'Inserting attachments ...<br />';
		@ob_flush(); flush();
		$this->_insertAttachments();
		echo 'Inserting event dates ...<br />';
		@ob_flush(); flush();
		$this->_insertEventDates();
		echo 'Inserting users ...<br />';
		@ob_flush(); flush();
		$this->_insertUsers();
		$this->_outputResults();
	}
	
	function _getFiles() {
		$this->files = array();
		$uploadsInfo = wp_upload_dir();
		if ($handle = opendir($uploadsInfo['basedir'] . '/oldsite_assets')) {
			while (false !== ($file = readdir($handle))) {
				if ($file == '.' || $file == '..') { continue; }
				$basename = preg_replace('/(.*)(\-\d+[x]\d+)(.*)/', '$1$3', $file);
				$basename = str_replace('JPG', 'jpg', $basename);
				
		        if (strpos($file, 'x300') !== false || strpos($file, '300x') !== false) {
		        	$this->files[$basename]['300'] = $file;
		        }else  if (strpos($file, 'x120') !== false || strpos($file, '120x') !== false) {
		        	$this->files[$basename]['120'] = $file;
				} else {
					$this->files[$basename]['100'] = $file;
				}
		    }
		}
	}
	
	function _getCategories() {
		global $table_prefix;
		
		while (list($k, $v) = each($this->categories)) {
			$q = 'SELECT term_id FROM ' . $table_prefix . 'terms WHERE slug="' . $v . '"';
			$res = mysql_query($q);
			if ($res && $r = mysql_fetch_assoc($res)) {
				$this->categoryNumbers[$k] = $r['term_id'];
			}
		}
	}
	
	function _outputResults() {
		echo '<div class="updated fade" id="message" style="background-color: rgb(255, 251, 204); width: 300px; margin-left: 20px">';
		echo '<p>Data <strong>copied</strong>.</p>';
		echo '<p>' . $this->successfulCount .' data copied, ' . $this->attachmentCount . ' attached</p>';
		echo '<p>' . $this->successfulUsersCount .' users copied</p>';
		echo '<p>' . $this->errorCount . ' errors</p>';
		if ($this->errorCount) {
			echo '<pre>' . print_r($this->errors) . '</pre>';
		}
		echo '</div>';
	}
	
	function _convertEvents() {
		
		$q = "SELECT * FROM bt_events"; 
		
		$oldData = mysql_query($q);
		
		$queries = array();
		$attachments = array();
		
		while ($oldDbRecord = mysql_fetch_assoc($oldData)) {
			$post = array();
			$post['ID'] = $oldDbRecord['id'];
			$post['post_author'] = $oldDbRecord['user_id'];
			$post['post_date'] = $oldDbRecord['create_date'];
			$post['post_date_gmt'] = date('Y-m-d H:i:s', strtotime($oldDbRecord['create_date']) + 21600);
			$post['post_title'] = $oldDbRecord['title'];
			$post['post_content'] = $oldDbRecord['summary'] . $oldDbRecord['text'];
			$post['post_excerpt'] = strip_tags($oldDbRecord['summary']);
			if ($oldDbRecord['status'] == 'enabled') {
				$post['post_status'] = 'publish';
			} else {
				$post['post_status'] = 'unpublished';
			}
			$post['post_name'] = preg_replace('/[^a-z0-9]/i', '-', trim(strtolower($oldDbRecord['title'])));
			$post['post_modified'] = $oldDbRecord['modify_date'];
			$post['post_modified_gmt'] = date('Y-m-d H:i:s', strtotime($oldDbRecord['modify_date']) + 21600);
			$post['post_parent'] = 0;
			$post['menu_order'] = 0;
			$post['post_type'] = 'post';
			$post['post_category'] = array($this->categoryNumbers[$oldDbRecord['category']]);
			if ($oldDbRecord['spotlight']) {
				$post['tags_input'] = array('spotlight');
			}
			if (trim($oldDbRecord['register'])) {
				$post['theatreevents-master-allow-reservations'] = 1;
			}
			
			// get images
			$this->_createImageAttachmentsForPost($oldDbRecord);
			// get files
			$this->_createFileAttachmentsForPost($oldDbRecord);

			$this->_createEventDates($oldDbRecord);

			$this->posts[] = $post;
			
		}
		
	}
	
	function _convertNews() {
		global $table_prefix;
		
		$newsItems = array();
		
		$q = "SELECT * FROM bt_news"; 
		
		$oldData = mysql_query($q);
		$attachments = array();
		
		while ($oldDbRecord = mysql_fetch_assoc($oldData)) {
			switch ($oldDbRecord['category']) {
				case 'inside':
					$status = 'private'; 
					break;
				default:
					$status = 'publish';
					break;
			}
			
			if ($oldDbRecord['status'] == 'disabled') {
				$status = 'draft';
			}
			
			$oldDbRecord['id'] = $oldDbRecord['id'] + 5000;
			
			$post = array(
				'ID' => $oldDbRecord['id'],
				'post_author' => $oldDbRecord['user_id'], //The user ID number of the author.
				'post_category' => array($this->categoryNumbers['news']), //Add some categories.
				'post_content' => $oldDbRecord['summary'] . '<br /><br />' . $oldDbRecord['text'], //The full text of the post.
				'post_date' => $oldDbRecord['date_line'], //The time post was made.
				'post_date_gmt' => date('Y-m-d H:i:s', strtotime($oldDbRecord['date_line']) + 21600), //The time post was made, in GMT.
				'post_status' => $status, //Set the status of the new post.
				'post_title' => $oldDbRecord['title'], //The title of your post.
				'post_type' => 'post' //Sometimes you want to post a page.
			);

			$this->posts[] = $post;
			
			$this->_createImageAttachmentsForPost($oldDbRecord);
			$this->_createFileAttachmentsForPost($oldDbRecord);
		}
	}
	
	function _convertPages() {
		
		$q = "SELECT * FROM bt_messages"; 
		
		$oldData = mysql_query($q);
		
		$queries = array();
		$attachments = array();
		
		while ($oldDbRecord = mysql_fetch_assoc($oldData)) {
			$post = array();
			$post['ID'] = $oldDbRecord['id'];
			$post['post_author'] = $oldDbRecord['user_id'];
			$post['post_date'] = $oldDbRecord['create_date'];
			$post['post_date_gmt'] = date('Y-m-d H:i:s', strtotime($oldDbRecord['create_date']) + 21600);
			$post['post_title'] = $oldDbRecord['title'];
			$post['post_content'] = $oldDbRecord['summary'] . $oldDbRecord['text'];
			$post['post_excerpt'] = strip_tags($oldDbRecord['summary']);
			if ($oldDbRecord['status'] == 'enabled') {
				$post['post_status'] = 'publish';
			} else {
				$post['post_status'] = 'unpublished';
			}
			$post['post_name'] = preg_replace('/[^a-z0-9]/i', '-', trim(strtolower($oldDbRecord['title'])));
			$post['post_modified'] = $oldDbRecord['modify_date'];
			$post['post_modified_gmt'] = date('Y-m-d H:i:s', strtotime($oldDbRecord['modify_date']) + 21600);
			$post['post_parent'] = 0;
			$post['menu_order'] = 0;
			$post['post_type'] = 'page';
			$post['post_category'] = array($this->categoryNumbers[$oldDbRecord['category']]);
			if ($oldDbRecord['spotlight']) {
				$post['tags_input'] = array('spotlight');
			}
			
			// get images
			$this->_createImageAttachmentsForPost($oldDbRecord);
			// get files
			$this->_createFileAttachmentsForPost($oldDbRecord);

			$this->_createEventDates($oldDbRecord);

			$this->posts[] = $post;
			
		}
		
	}
	
	function _convertUsers() {
		$q = "SELECT * FROM bt_users"; // LIMIT 10";
		$oldData = mysql_query($q);
		while ($oldDbRecord = mysql_fetch_assoc($oldData)) {
			$user = array(
				'ID' => $oldDbRecord['id'],
				'user_login' => $oldDbRecord['username'],
				'user_pass' => 'westbank',
				'user_nicename' => $oldDbRecord['f_name'] . ' ' . $oldDbRecord['l_name'],
				'user_url' => $oldDbRecord['plink'],
				'user_email' => $oldDbRecord['email'],
				'display_name' => $oldDbRecord['f_name'] . ' ' . $oldDbRecord['l_name'],
				'first_name' => $oldDbRecord['f_name'],
				'last_name' => $oldDbRecord['l_name'],
				'description' => $oldDbRecord['bio'],
				'user_registered' => $oldDbRecord['create_date']
			);
			
			$this->users[] = $user;
		}
	}
	
	function _getImageFromOldDbId($id) {
		$imgResult = mysql_query('SELECT * FROM bt_images WHERE id=' . $id);
		if ($imgResult && $imgR = mysql_fetch_assoc($imgResult)) {
			if ($imgR['fname']) {
				$imgR['fname'] = preg_replace('/\.(.*\.)/', '$1', $imgR['fname']);
				$imgR['fname'] = preg_replace('/\.(.*\.)/', '$1', $imgR['fname']);
				$imgR['fname'] = preg_replace('/\.(.*\.)/', '$1', $imgR['fname']);
				$imgR['fname'] = preg_replace('/\-\./', '.', $imgR['fname']);
				$imgR['fname'] = preg_replace('/jpg\./', '.', $imgR['fname']);
				$imgR['fname'] = preg_replace('/495_d.*ive2/', '495_derive2', $imgR['fname']);
				$imgR['fname'] = preg_replace('/[!~\(\)$]+/', '', $imgR['fname']);
				$imgR['fname'] = preg_replace('/[&!\s\-\,]+/', '-', $imgR['fname']);
				$imgR['fname'] = preg_replace('/\-\./', '.', $imgR['fname']);
				$imgR['fname'] = preg_replace('/\.jpeg/', '.jpg', $imgR['fname']);
				
				return $imgR;
			}
		}
		
		return false;
	}
	
	function _createImageAttachmentsForPost($oldDbRecord) {
		$attachments = array();
		$arrayKeys = array_keys($this->files);
		for ($i = 1; isset($oldDbRecord["image$i"]) && $oldDbRecord["image$i"]; $i++) {
			// get image info
			$image = $this->_getImageFromOldDbId($oldDbRecord["image$i"]);
			$image['fname'] = str_replace('JPG', 'jpg', $image['fname']);
			if ($image && trim($image['fname'])) {
				if (!isset($this->files[$image['fname']]['300']) && !isset($this->files[$image['fname']]['120'])) {
					//echo 'could not find ' . $image['fname'] . '<br />';
					$probableResult = $this->_fuzzyFind($image['fname'], $arrayKeys, 3);
					if ($probableResult) {
						$image['fname'] = $probableResult;
					} else {
						_e('<blockquote>Could not create an image attachment for <strong>' . $image['fname'] . '</strong> because no ' .
							'corresponding image file was found. This file may not be an image.</blockquote><br />');
						continue;
					}
				}
				$this->attachments[] = array( 
					array(
						'post_title' => preg_replace('/\.jpg/i', '', $image['fname']),
						'post_content' => '',
						'post_status' => 'inherit',
						'post_type' => 'attachment',
						'post_parent' => $oldDbRecord['id'],
						'post_mime_type' => 'image/jpeg'
					),
					$_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/oldsite_assets/' . end($this->files[$image['fname']]),
					$oldDbRecord['id']
				);
			}
		}
	}
	
	function _createFileAttachmentsForPost($oldDbRecord) {
		$attachments = array();
		for ($i = 1; isset($oldDbRecord["file$i"]) && $oldDbRecord["file$i"]; $i++) {
			$fileResult = mysql_query('SELECT * FROM bt_files WHERE id=' . $oldDbRecord["file$i"]);
			if ($fileResult && $fileR = mysql_fetch_assoc($fileResult)) {
				$this->attachments[] = array( 
					array(
						'post_title' => $fileR['fname'],
						'post_content' => '',
						'post_status' => 'inherit',
						'post_type' => 'attachment',
						'post_parent' => $oldDbRecord['id']
					),
					$_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/oldsite_assets/' . $fileR['fname'],
					$oldDbRecord['id']
				);
			}
		}
	}
	
	function _createEventDates($oldDbRecord) {
		global $table_prefix;
		for ($i = 1; $i <= 20; $i++) {
			if ($oldDbRecord["date$i"] && $oldDbRecord["date$i"] !== '0000-00-00 00:00:00') {
				$allowReservations = 0;
				if (trim($oldDbRecord['register'])) {
					$allowReservations = 1;
				}
				$this->eventDates[] = 'INSERT INTO ' . $table_prefix . 'theatreevents (' . 
								'post_id, eventdate, price, comment, allow_reservations' . 
								') VALUES (' . 
								$oldDbRecord['id'] . ", '" . $oldDbRecord["date$i"] . "', '" . $oldDbRecord["price$i"] . "', '" . addslashes(stripslashes(stripslashes(stripslashes($oldDbRecord["comment$i"])))) . "', $allowReservations" . 
								'); ';
			}
		}
	}
	
	function _clearExistingAttachmentsAndDates() {
		global $wpdb, $table_prefix;
		foreach ($this->posts as $post) {
			$currentAttachments =& get_children( array(
				'post_type' => 'attachment',
				'post_parent' => $post['ID']
			));
			
			if ($currentAttachments) {
				foreach ( $currentAttachments as $attachment_id => $attachment ) {
					wp_delete_attachment( $attachment_id, 'full' );
				}
			}
			
			$wpdb->query('DELETE FROM ' . $table_prefix . 'theatreevents WHERE post_id=' . $post['ID']);
		}
	}
	
	function _insertAttachments() {
		foreach($this->attachments as $attachment) {
			$attach_id = wp_insert_attachment($attachment[0], $attachment[1], $attachment[2]);
			$attach_data = wp_generate_attachment_metadata($attach_id, $attachment[1]);
			if (wp_update_attachment_metadata($attach_id, $attach_data)) {
				$this->attachmentCount++;
			}
		}
	}
	function _insertPosts() {
		global $wpdb, $table_prefix;
		foreach ($this->posts as $post) {
			$possiblePost = get_post($post['ID']);
			$post_id = 0;
			if (!$possiblePost) { // if no post with that ID, create a record with that ID
				echo 'creating for ' . $post['ID'];
				@ob_flush(); flush();
				$wpdb->insert($table_prefix . 'posts', array('ID' => $post['ID']));
			}
			echo 'updating ' . $post['post_title'] . '<br />';
			@ob_flush(); flush();
			$post_id = wp_update_post($post);
			
			if ($post_id) {
				$this->successfulCount++;
			}
		}
	}
	
	function _insertEventDates() {
		global $wpdb;
		foreach ($this->eventDates as $eventDate) {
			$wpdb->query($eventDate);
		}
	}
	
	function _insertUsers() {
		global $wpdb, $table_prefix;
		foreach ($this->users as $user) {
			$possibleUser = get_userdata($user['ID']);
			$user_id = 0;
			if (!$possibleUser) { // if no user with that ID, create a record with that ID
				echo 'creating for ' . $user['ID'];
				@ob_flush(); flush();
				$wpdb->insert($table_prefix . 'users', array('ID' => $user['ID']));
			}
			echo 'updating ' . $user['user_login'] . '<br />';
			@ob_flush(); flush();
			$user_id = wp_update_user($user);
			
			if ($user_id) {
				$this->successfulUsersCount++;
			}
		}
	}
	
	function _fuzzyFind($needle, $haystack, $delta) {
		$fuzzy = new Approximate_Search($needle, $delta);
		$probableResult = '';
		foreach ($haystack as $candidate) {
			$matches = $fuzzy->search($candidate);
			if (count($matches)) {
				$probableResult = $candidate;
				break;
			}
		}
		return $probableResult;
	}
	
}