<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<div id="footer">
	<a href="<?php echo get_option('home'); ?>/">home</a>
	<?php /* <a href="<?php echo get_option('home'); ?>/category/events">events</a> */ ?>
	<a href="<?php echo get_option('home'); ?>/pages/donate">donate</a>
	<a href="<?php echo get_option('home'); ?>/pages/social">social</a>
	<a href="<?php echo get_option('home'); ?>/pages/about">inside bedlam</a>
	<a href="<?php echo get_option('home'); ?>/category/news">news</a>
	<a href="<?php echo get_option('home'); ?>/pages/contact">contact</a>
	
	<a href="<?php echo get_option('home'); ?>/pages/directions">directions to bedlam</a>
	
	<br />
	
	&copy; <?php echo date('Y'); ?> Bedlam Theatre <?php $btOptions = get_option('bedlamtheatre'); echo $btOptions['telephone']; ?>
</div>
</div>


<?php wp_footer(); ?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-417429-1");
pageTracker._setDomainName(".bedlamtheatre.org");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
